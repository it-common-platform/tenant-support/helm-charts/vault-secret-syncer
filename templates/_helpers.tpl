{{/*
Expand the name of the chart.
*/}}
{{- define "..name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "..fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "..chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "..labels" -}}
helm.sh/chart: {{ include "..chart" . }}
{{ include "..selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "..selectorLabels" -}}
app.kubernetes.io/name: {{ include "..name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "..serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "..fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/* Job Spec */}}
{{- define "..jobSpec" -}}
spec:
  template:
    metadata:
      labels:
        {{- include "..selectorLabels" . | nindent 8 }}
    spec:
      serviceAccountName: {{ .Values.serviceAccount.name }}
      restartPolicy: "Never"
      containers:
        - name: {{ .Chart.Name }}
          image: {{ .Values.job.image }}
          command: ["bash", "-c", "/scripts/script.sh"]
          resources:
            requests:
              memory: 32Mi
              cpu: 100m
          securityContext:
            runAsNonRoot: true
            runAsUser: {{ .Values.securityContext.runAsUser | int }}
            runAsGroup: {{ .Values.securityContext.runAsGroup | int }}
          volumeMounts:
            - name: scripts
              mountPath: /scripts
          env:
            - name: RELEASE_NAME
              value: {{ .Release.Name }}
            - name: RELEASE_NAMESPACE
              value: {{ .Release.Namespace }}
            - name: VAULT_ADDR
              value: {{ required "A Vault address must be provided" .Values.vault.address }}
            - name: VAULT_AUTH_PATH
              value: {{ required "A Vault auth path must be speicified" .Values.vault.authPath }}
            - name: VAULT_ROLE
              value: {{ required "A Vault role must be provided" .Values.vault.role }}
            {{- if .Values.debug }}
            - name: DEBUG
              value: "true"
            {{- end }}
            {{- range $i, $item := .Values.registry }}
            - name: REGISTRY_NAME_{{ $i }}
              value: {{ required "A secret name must be provided" $item.name }}
            - name: REGISTRY_URL_{{ $i }}
              value: {{ $item.registryUrl | default "code.vt.edu:5005" }}
            - name: REGISTRY_PATH_{{ $i }}
              value: {{ required "A secret path must be provided" $item.vaultSecretPath }}
            - name: REGISTRY_USERNAME_KEY_{{ $i }}
              value: {{ $item.vaultUsernameKey | default "username" }}
            - name: REGISTRY_PASSWORD_KEY_{{ $i }}
              value: {{ $item.vaultPasswordKey | default "password "}}
            {{- end }}
            {{- range $i, $item := .Values.secrets }}
            - name: SECRET_NAME_{{ $i }}
              value: {{ required "A secret name must be provided" $item.name }}
            - name: SECRET_PATH_{{ $i }}
              value: {{ required "A secret path must be provided" $item.vaultSecretPath }}
            {{- range $j, $key := $item.keys }}
            - name: SECRET_KEY_{{ $i }}_{{ $j }}
              value: {{ required "A secret key must be provided" $key.secretKey }}
            - name: SECRET_VAULT_KEY_{{ $i }}_{{ $j  }}
              value: {{ required "A Vault key must be provided" $key.vaultKey }}
            {{- end }}
            {{- end }}
      volumes:
        - name: scripts
          configMap:
            name: {{ .Values.configMap.name }}
            defaultMode: 0755
{{- end }}
