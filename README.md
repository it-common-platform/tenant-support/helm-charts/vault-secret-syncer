# Vault Secret Syncer

A Helm chart that syncs secrets from Vault into Kubernetes secrets. While most applications can be configured to pull secrets from Vault either directly or using sidecars, there are times when the secrets need to be stored as Kubernetes secrets (such as supporting `imagePullSecrets`).

This chart does so by defining a Job/CronJob that will run a Pod that fetches the secrets and stores them as a Kubernetes secret.

## Pre-requisites

In order to fetch secrets from Vault, authentication with Vault is required. This chart leverages the Kubernetes auth method when authenticating to Vault. Therefore, a Vault role needs to be defined that can be assumed using the ServiceAccount created by this chart. 

## Configuration

While the bundled `values.yaml` provides many of the defaults, there are specifics that need to be provided. Therefore, you will need to provide the following details in your own values.yaml override. Additional overrides are documented in the [values.yaml](./values.yaml).

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `serviceAccount.name` | The name of the service account to use. | `"vault-secret-syncer"` |
| `serviceAccount.create` | Whether to create the service account or not (should pre-exist with necessary RBAC if false) | `true` |
| `configMap.name` |  | `"vault-secret-syncer-files"` |
| `job.image` |  | `"code.vt.edu:5005/it-common-platform/tenant-support/images/aws-kubectl:latest"` |
| `cronJob.schedule` | How frequently to refresh the secret from Vault | `"0 3 * * *"` |
| `vault.address` |  | `"https://vault.es.cloud.vt.edu:8200"` |
| `vault.authPath` | One of platform-prod, platform-pprd, platform-op-prod, platform-op-pprd | `"platform-prod"` |
| `vault.role` | The Vault role the Job should assume/authenticate as | `"pipeline-placeholder-role"` |
| `registry` | To sync registry secrets | `[]` |
| `secrets` | To sync generic secrets | `[]` |