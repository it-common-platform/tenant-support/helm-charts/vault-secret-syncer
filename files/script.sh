#!/bin/bash

set -e

if [ $VAULT_ADDR == "" ]; then
  echo "Error: No VAULT_ADDR defined"
  exit 1
fi

if [ $VAULT_ROLE == "" ]; then
  echo "Error: No VAULT_ROLE defined"
  exit 1
fi

if [ $VAULT_AUTH_PATH == "" ]; then
  echo "Error: No VAULT_AUTH_PATH defined"
  exit 1
fi

if [ -n "$DEBUG" ]; then
  set -x
fi

SA_TOKEN=$(cat /run/secrets/kubernetes.io/serviceaccount/token)

echo "- Authenticating as role ${VAULT_ROLE} against ${VAULT_ADDR}"
AUTH_REQUEST=$(curl -s -X POST -H "Content-Type: application/json" \
    --data "{\"role\":\"${VAULT_ROLE}\",\"jwt\":\"${SA_TOKEN}\"}" \
    $VAULT_ADDR/v1/auth/${VAULT_AUTH_PATH}/login)

ERRORS=$(echo $AUTH_REQUEST | jq -r .errors)
if [ "$ERRORS" != "null" ]; then
  echo "Error during Vault authentication: ${ERRORS}"
  exit 1
fi

echo "-- Successfully obtained token"
VAULT_TOKEN=$(echo $AUTH_REQUEST | jq -r .auth.client_token)

# Add annotations to a secret so it will be controlled by the Helm Chart
annotateSecret() {
  local secret="$1"
  if [ -z "$secret" ]; then
    echo "Secret name required"
    exit 1
  fi
  echo "- Annotate and label secret"
  kubectl annotate secret $secret meta.helm.sh/release-name=${RELEASE_NAME}
  kubectl annotate secret $secret meta.helm.sh/release-namespace=${RELEASE_NAMESPACE}
  kubectl label secret $secret app.kubernetes.io/managed-by=Helm
}

waitForSecret() {
  # just sleep and return because the kubectl get secret command is failing
  sleep 30
  return
  local secret="$1"
  if [ -z "$secret" ]; then
    echo "Secret name required"
    exit 1
  fi
  count=0
  while [ $count -lt 10 ]; do
    kubectl get secret "$secret" >/tmp/kube-get-secret 2>&1 && rm -f /tmp/kube-get-secret && return
    [ -n "${DEBUG}" ] && cat /tmp/kube-get-secret
    count=$(($count + 1))
    sleep 3
  done
}

isEmpty() {
  local secret="$1"
  if [ -n "${secret}" -a "${secret}" != "null" ]; then
    echo "$secret"
  fi
}

##
### Registry Secrets
for key in ${!REGISTRY_NAME_*}; do
  index=${key#REGISTRY_NAME_}
  urlVar="REGISTRY_URL_${index}"
  pathVar="REGISTRY_PATH_${index}"
  usernameKeyVar="REGISTRY_USERNAME_KEY_${index}"
  passwordKeyVar="REGISTRY_PASSWORD_KEY_${index}"
  name=${!key}
  url=${!urlVar}
  path=${!pathVar}
  usernameKey=${!usernameKeyVar}
  passwordKey=${!passwordKeyVar}

  set +e
  echo "- Fetching Vault secret data $name"
  secret=$(curl -s -f -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/${path})
  username=$(echo $secret | jq -r ".data.data.${usernameKey}")
  password=$(echo $secret | jq -r ".data.data.${passwordKey}")
  if [ -z "${username}" ]; then
    echo "Unable to fetch '${usernameKey}' from '${name}'"
    exit 1
  fi
  if [ -z "${password}" ]; then
    echo "Unable to fetch '${passwordKey}' from '${name}'"
    exit 1
  fi
  set -e

  # Wait for Helm to create the secret from the manifest
  waitForSecret $name

  echo "- Deleting any existing secret (since upgrading isn't possible via kubectl)"
  kubectl delete secret $name || true

  echo "- Defining new secret"
  kubectl create secret docker-registry $name \
    --docker-server=$url \
    --docker-username=$username \
    --docker-password=$password \
    --docker-email=""

  annotateSecret $name
done

##
### Generic Secrets
for key in ${!SECRET_NAME_*}; do
  index=${key#SECRET_NAME_}
  pathVar="SECRET_PATH_${index}"
  name=${!key}
  url=${!urlVar}
  path=${!pathVar}

  set +e
  echo "- Fetching Vault secret data for $name"
  secret=$(curl -s -f -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/${path})
  if [ -z "$secret" ]; then
    echo "Unable to fetch secret '${path}':"
    curl -D- -s -f -H "X-Vault-Token: $VAULT_TOKEN" $VAULT_ADDR/v1/${path}
    exit 1
  fi
  set -e

  literals=""
  for secretKey in $(env | grep "^SECRET_KEY_${index}_" | cut -d= -f1|sort -n); do
    keyIndex=${secretKey#SECRET_KEY_${index}_}
    secretKeyVar="SECRET_KEY_${index}_${keyIndex}"
    secretVaultKeyVar="SECRET_VAULT_KEY_${index}_${keyIndex}"
    secretKey=${!secretKeyVar}
    secretVaultKey=${!secretVaultKeyVar}

    # Fetch the value for this key from the secret
    # Static secrets have a different path from other secrets
    # We'll try possible permutations until one works
    secretValue=$(echo $secret | jq -r ".${secretVaultKey}") || true
    [ -z "$(isEmpty ${secretValue})" ] && secretValue=$(echo $secret | jq -r ".data.${secretVaultKey}") || true
    [ -z "$(isEmpty ${secretValue})" ] && secretValue=$(echo $secret | jq -r ".data.data.${secretVaultKey}")

    if [ -z "${secretValue}" ]; then
      echo "Unable to fetch '${path}:${secretVaultKey}' from Vault"
      exit 1
    fi
    literals="$literals --from-literal=${secretKey}=${secretValue}"
  done

  # Wait for Helm to create the secret from the manifest
  waitForSecret $name

  echo "- Deleting any existing secret (since upgrading isn't possible via kubectl)"
  kubectl delete secret $name || true

  echo "- Defining new secret"
  kubectl create secret generic $name $literals

  annotateSecret $name
done
